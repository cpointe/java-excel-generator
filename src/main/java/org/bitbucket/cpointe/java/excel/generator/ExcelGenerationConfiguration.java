package org.bitbucket.cpointe.java.excel.generator;

import static org.bitbucket.cpointe.java.excel.generator.ExcelGenerationConstants.COLUMN_A;
import static org.bitbucket.cpointe.java.excel.generator.ExcelGenerationConstants.FIRST_ROW;

import java.io.File;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.apache.commons.lang3.StringUtils;
import org.apache.poi.ss.usermodel.CellStyle;
import org.apache.poi.ss.usermodel.Font;
import org.apache.poi.ss.usermodel.HorizontalAlignment;
import org.apache.poi.ss.util.CellAddress;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;

public class ExcelGenerationConfiguration {

    private static final int MINIMUM_TITLE_BUFFER_FOR_LOGO = 2;
    private String targetFolder = ".";
    private boolean targetFolderUseTempDir = true;
    private String targetFileName = "generated-excel-file";
    private String targetFileSuffix = ".xlsx";
    private String targetFileDateFormatSuffix = "yyyy-MM-dd_HH:mm:ss.SSS";
    private String sheetName = "Data";
    private boolean targetFileNameAddTimestamp = true;
    private File targetFile;
    private boolean includeHeaderRow = true;
    private List<ExcelColumnDefinition> columnDefinitions;
    private boolean headerRowHasAutoFilter = true;
    private String title;
    private int numberOfCellsForTitleOrDescription = 9;
    private CellStyle titleStyle;
    private short titleFontSize = 15;
    private String titleFontName = "Calibri";
    private int titleBuffer = 2;
    private int totalsBuffer = 2;
    private String description;
    private String logoLocation;
    private CellAddress logoPositioning;
    private CellStyle descriptionStyle;

    public File getTargetFile() throws IOException {
        File f;
        if (targetFile != null) {
            f = targetFile;
        } else if (isTargetFolderUseTempDir()) {
            f = File.createTempFile(getTargetFileName(), getTargetFileSuffix());
        } else {
            String fileNameBase = getTargetFolder() + File.separator + getTargetFileName();
            f = new File(fileNameBase + getTargetFileSuffix());
            if (isTargetFileNameAddTimestamp()) {
                String timestamp = new SimpleDateFormat(getTargetFileDateFormatSuffix()).format(new Date());
                f = new File(fileNameBase + "-" + timestamp + getTargetFileSuffix());
            }
        }

        return f;
    }

    public String getSheetName() {
        return sheetName;
    }

    public void setSheetName(String sheetName) {
        this.sheetName = sheetName;
    }

    public String getTargetFileDateFormatSuffix() {
        return targetFileDateFormatSuffix;
    }

    public void setTargetFileDateFormatSuffix(String targetFileDateFormatSuffix) {
        this.targetFileDateFormatSuffix = targetFileDateFormatSuffix;
    }

    public String getTargetFileSuffix() {
        return targetFileSuffix;
    }

    public void setTargetFileSuffix(String targetFileSuffix) {
        this.targetFileSuffix = targetFileSuffix;
    }

    public String getTargetFileName() {
        return targetFileName;
    }

    public void setTargetFileName(String targetFileName) {
        this.targetFileName = targetFileName;
    }

    public boolean isTargetFolderUseTempDir() {
        return targetFolderUseTempDir;
    }

    public void setTargetFolderUseTempDir(boolean targetFolderUseTempDir) {
        this.targetFolderUseTempDir = targetFolderUseTempDir;
    }

    public String getTargetFolder() {
        return targetFolder;
    }

    public void setTargetFolder(String targetFolder) {
        this.targetFolder = targetFolder;
    }

    public boolean isTargetFileNameAddTimestamp() {
        return targetFileNameAddTimestamp;
    }

    public void setTargetFileNameAddTimestamp(boolean targetFileNameAddTimestamp) {
        this.targetFileNameAddTimestamp = targetFileNameAddTimestamp;
    }

    public void setFile(File targetFile) {
        this.targetFile = targetFile;
    }

    public void setHeaders(String[] headers) {
        columnDefinitions = new ArrayList<>();
        for (int i = 0; i < headers.length; i++) {
            columnDefinitions.add(new ExcelColumnDefinition(i, headers[i]));
        }
    }

    public List<ExcelColumnDefinition> getColumnDefinitions() {
        return columnDefinitions;
    }

    public void setIncludeHeaderRow(boolean includeHeaderRows) {
        this.includeHeaderRow = includeHeaderRows;
    }

    public boolean shouldIncludeHeaderRow() {
        return includeHeaderRow && getColumnDefinitions() != null && !getColumnDefinitions().isEmpty();
    }

    public void setHeaderRowHasAutoFilter(boolean headerRowHasAutoFilter) {
        this.headerRowHasAutoFilter = headerRowHasAutoFilter;
    }

    public boolean headerRowHasAutoFilter() {
        return headerRowHasAutoFilter;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getTitle() {
        return title;
    }

    public int getNumberOfCellsForTitleOrDescription() {
        return numberOfCellsForTitleOrDescription;
    }

    public void setNumberOfCellsForTitle(int numberOfCellsForTitle) {
        this.numberOfCellsForTitleOrDescription = numberOfCellsForTitle;
    }

    public CellStyle getTitleStyle(XSSFWorkbook wb) {
        if (titleStyle == null) {
            titleStyle = wb.createCellStyle();
            Font headerFont = wb.createFont();
            headerFont.setBold(true);
            titleStyle.setAlignment(HorizontalAlignment.LEFT);
            headerFont.setFontHeightInPoints(getTitleFontSize());
            headerFont.setFontName(getTitleFontName());
            titleStyle.setFont(headerFont);
        }
        return titleStyle;
    }

    protected String getTitleFontName() {
        return titleFontName;
    }

    protected void setTitleFontName(String titleFontName) {
        this.titleFontName = titleFontName;
    }

    protected short getTitleFontSize() {
        return titleFontSize;
    }

    protected void setTitleFontSize(short titleFontSize) {
        this.titleFontSize = titleFontSize;
    }

    public boolean shouldIncludeTitleBuffer() {
        return !StringUtils.isBlank(title) || !StringUtils.isBlank(description) || !StringUtils.isBlank(logoLocation);
    }

    public int getTitleBuffer() {
        if (!StringUtils.isBlank(logoLocation) && titleBuffer < MINIMUM_TITLE_BUFFER_FOR_LOGO) {
            return MINIMUM_TITLE_BUFFER_FOR_LOGO;
        }
        return titleBuffer;
    }

    public void setTitleBuffer(int titleBuffer) {
        this.titleBuffer = titleBuffer;
    }

    public void setDesctiprion(String description) {
        this.description = description;
    }

    public String getDescription() {
        return description;
    }

    public void setLogoLocation(String logoLocation) {
        this.logoLocation = logoLocation;
    }

    public String getLogoLocation() {
        return logoLocation;
    }

    public CellAddress getLogoPositioning() {
        if (logoPositioning == null) {
            // default to adding the logo right after the title & description
            logoPositioning = new CellAddress(FIRST_ROW, COLUMN_A);
        }
        return logoPositioning;
    }

    public void setLogoPostioning(CellAddress logoPositioning) {
        this.logoPositioning = logoPositioning;
    }

    public CellStyle getDescriptionStyle(XSSFWorkbook workbook) {
        if(descriptionStyle == null) {
            descriptionStyle = workbook.createCellStyle();
            descriptionStyle.setWrapText(true);
        }
        return descriptionStyle;
    }

    public int getTotalsBuffer() {
        return totalsBuffer;
    }
    
    public void setTotalsBuffer(int totalsBuffer) {
        this.totalsBuffer = totalsBuffer;
    }

    public boolean hasTotalColumns() {
        boolean containsColumnWithTotal = false;
        if(getColumnDefinitions() != null) {
            for(ExcelColumnDefinition columnDefinition: getColumnDefinitions()) {
                if(!StringUtils.isBlank(columnDefinition.getColumnTotalFormula()) ) {
                    containsColumnWithTotal = true;
                }
            }    
        }
        return containsColumnWithTotal;
    }
}
