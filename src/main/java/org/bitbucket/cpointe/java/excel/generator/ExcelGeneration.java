package org.bitbucket.cpointe.java.excel.generator;

import static org.bitbucket.cpointe.java.excel.generator.ExcelGenerationConstants.COLUMN_A;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.Date;
import java.util.List;

import org.apache.commons.io.FileUtils;
import org.apache.commons.io.IOUtils;
import org.apache.commons.lang3.StringUtils;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.CellStyle;
import org.apache.poi.ss.usermodel.ClientAnchor;
import org.apache.poi.ss.usermodel.CreationHelper;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.ss.util.CellRangeAddress;
import org.apache.poi.xssf.usermodel.XSSFCell;
import org.apache.poi.xssf.usermodel.XSSFDrawing;
import org.apache.poi.xssf.usermodel.XSSFPicture;
import org.apache.poi.xssf.usermodel.XSSFRow;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;

public class ExcelGeneration {

    public static final Logger LOGGER = LogManager.getLogger(ExcelGeneration.class);

    public static ExcelGenerationResult generate(ExcelGenerationConfiguration configuration, Object[][] data)
            throws IOException {
        ExcelGenerationResult result = new ExcelGenerationResult();

        XSSFWorkbook workbook = new XSSFWorkbook();
        XSSFSheet sheet = workbook.createSheet(configuration.getSheetName());

        generateLogo(workbook, sheet, configuration, result);
        generateTitle(workbook, sheet, configuration, result);
        generateDescription(workbook, sheet, configuration, result);
        generateTitleBuffer(workbook, sheet, configuration, result);
        generateHeaderRow(workbook, sheet, configuration, result);
        generateDataRows(workbook, sheet, configuration, result, data);
        generateTotalRows(workbook, sheet, configuration, result, data);
        enableFiltersInHeaderRow(workbook, sheet, configuration, result);

        writeWorkbookToFile(configuration, result, workbook);

        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("Generated " + result.getNumberOfDataRowsGenerated() + " rows in "
                    + result.getGeneratedFile().getAbsolutePath());
        }
        return result;
    }

    private static void generateTotalRows(XSSFWorkbook workbook, XSSFSheet sheet,
            ExcelGenerationConfiguration configuration, ExcelGenerationResult result, Object[][] data) {
        if (configuration.hasTotalColumns()) {
            generateTotalsHeaders(workbook, sheet, configuration, result);
            generateTotalsFormulas(workbook, sheet, configuration, result);
        }
    }

    private static void generateTotalsHeaders(XSSFWorkbook workbook, XSSFSheet sheet,
            ExcelGenerationConfiguration configuration, ExcelGenerationResult result) {

        result.incrementTotalRowsGenerated(configuration.getTotalsBuffer());
        Row row = createNewRow(sheet, result);

        // set styles for all cells in row for nice visual effect
        CellStyle totalsStyle = configuration.getColumnDefinitions().get(COLUMN_A).getTotalStyle(workbook);
        setStylesForAllColumnsInRow(configuration, row, totalsStyle);
        
        for (ExcelColumnDefinition columnDefinition : configuration.getColumnDefinitions()) {
            String formula = columnDefinition.getColumnTotalFormula();
            if (!StringUtils.isBlank(formula)) {
                String header = ExcelGenerationConstants.getFormulaHeader(formula);
                row.getCell(columnDefinition.getPositionIndex()).setCellValue(header);
            }
        }
    }

    private static void setStylesForAllColumnsInRow(ExcelGenerationConfiguration configuration, Row row,
            CellStyle totalsStyle) {
        for (ExcelColumnDefinition columnDefinition : configuration.getColumnDefinitions()) {
            Cell cell = row.getCell(columnDefinition.getPositionIndex());
            if (cell == null) {
                cell = row.createCell(columnDefinition.getPositionIndex());
            }
            cell.setCellStyle(totalsStyle);
        }
    }

    private static void generateTotalsFormulas(XSSFWorkbook workbook, XSSFSheet sheet,
            ExcelGenerationConfiguration configuration, ExcelGenerationResult result) {

        Row row = createNewRow(sheet, result);

        // set styles for all cells in row for nice visual effect
        CellStyle totalsStyle = configuration.getColumnDefinitions().get(COLUMN_A).getTotalStyle(workbook);
        setStylesForAllColumnsInRow(configuration, row, totalsStyle);

        for (ExcelColumnDefinition columnDefinition : configuration.getColumnDefinitions()) {
            if (!StringUtils.isBlank(columnDefinition.getColumnTotalFormula())) {
                Cell cellWithTotal = row.getCell(columnDefinition.getPositionIndex());
                cellWithTotal.setCellStyle(columnDefinition.getTotalStyle(workbook));

                XSSFRow firstDataRow = sheet.getRow(result.getFirstDataRowIndex());
                XSSFCell topCell = firstDataRow.getCell(columnDefinition.getPositionIndex());

                XSSFRow lastDataRow = sheet.getRow(result.getLastDataRowIndex());
                XSSFCell bottomCell = lastDataRow.getCell(columnDefinition.getPositionIndex());

                String dataCellRange = topCell.getReference() + ExcelGenerationConstants.COLON
                        + bottomCell.getReference();

                String fomulaString = columnDefinition.getColumnTotalFormula() + ExcelGenerationConstants.OPEN_PAREN
                        + dataCellRange + ExcelGenerationConstants.CLOSE_PAREN;

                cellWithTotal.setCellFormula(fomulaString);
            }
        }
    }

    private static void generateLogo(XSSFWorkbook workbook, XSSFSheet sheet, ExcelGenerationConfiguration configuration,
            ExcelGenerationResult result) {
        String logoLocation = configuration.getLogoLocation();
        if (!StringUtils.isBlank(logoLocation)) {
            File logoFile = new File(logoLocation);
            if (logoFile.exists()) {
                // add picture data to this workbook.
                Integer pictureIdx = addPictureToWorkbook(workbook, logoFile);
                if (pictureIdx != null) {
                    CreationHelper helper = workbook.getCreationHelper();

                    // Create the drawing patriarch. This is the top level
                    // container
                    // for all shapes.
                    XSSFDrawing drawing = sheet.createDrawingPatriarch();

                    // add a picture shape
                    ClientAnchor anchor = helper.createClientAnchor();
                    // set top-left corner of the picture
                    anchor.setCol1(configuration.getLogoPositioning().getColumn());
                    anchor.setRow1(configuration.getLogoPositioning().getRow());
                    XSSFPicture picture = drawing.createPicture(anchor, pictureIdx);
                    // auto-size picture relative to its top-left corner
                    picture.resize();
                    result.setLogoAdded(true);
                } else {
                    LOGGER.warn("Failed to add the logo to the spreadsheet");
                }
            } else {
                LOGGER.warn("Failed to find desired logo (" + logoFile.getAbsolutePath()
                        + "). Going to proceed without the logo.");
            }
        }
    }

    private static Integer addPictureToWorkbook(XSSFWorkbook workbook, File logoFile) {
        Integer pictureIndex = null;
        try {
            InputStream is = new FileInputStream(logoFile);
            byte[] bytes = IOUtils.toByteArray(is);
            pictureIndex = workbook.addPicture(bytes, Workbook.PICTURE_TYPE_JPEG);
            is.close();
        } catch (FileNotFoundException e) {
            LOGGER.error("Failed to find logo " + logoFile.getAbsolutePath(), e);
        } catch (IOException e) {
            LOGGER.error("Failed to read logo " + logoFile.getAbsolutePath(), e);
        }
        return pictureIndex;
    }

    private static Cell createMergedCellForTitleOrDescription(XSSFSheet sheet,
            ExcelGenerationConfiguration configuration, ExcelGenerationResult result, Row row) {
        int startingColumn = COLUMN_A;
        if (result.isLogoAdded()) {
            startingColumn++;
        }

        Cell cell = row.createCell(startingColumn);
        CellRangeAddress region = new CellRangeAddress(row.getRowNum(), row.getRowNum(), startingColumn,
                startingColumn + configuration.getNumberOfCellsForTitleOrDescription());
        sheet.addMergedRegion(region);
        return cell;
    }

    private static void generateTitleBuffer(XSSFWorkbook workbook, XSSFSheet sheet,
            ExcelGenerationConfiguration configuration, ExcelGenerationResult result) {
        if (configuration.shouldIncludeTitleBuffer()) {
            result.incrementTotalRowsGenerated(configuration.getTitleBuffer());
        }
    }

    private static void generateDescription(XSSFWorkbook workbook, XSSFSheet sheet,
            ExcelGenerationConfiguration configuration, ExcelGenerationResult result) {
        String description = configuration.getDescription();
        CellStyle descriptionStyle = configuration.getDescriptionStyle(workbook);
        generateMergedTileOrDescriptionCell(sheet, configuration, result, description, descriptionStyle);
    }

    private static void generateTitle(XSSFWorkbook workbook, XSSFSheet sheet,
            ExcelGenerationConfiguration configuration, ExcelGenerationResult result) {
        String title = configuration.getTitle();
        CellStyle titleStyle = configuration.getTitleStyle(workbook);

        generateMergedTileOrDescriptionCell(sheet, configuration, result, title, titleStyle);
    }

    private static void generateMergedTileOrDescriptionCell(XSSFSheet sheet, ExcelGenerationConfiguration configuration,
            ExcelGenerationResult result, String titleOrDescriptionValue, CellStyle titleorDescriptionStyle) {
        if (!StringUtils.isBlank(titleOrDescriptionValue)) {
            Row row = createNewRow(sheet, result);

            Cell cell = createMergedCellForTitleOrDescription(sheet, configuration, result, row);

            cell.setCellValue(titleOrDescriptionValue);
            cell.setCellStyle(titleorDescriptionStyle);

        }
    }

    private static Row createNewRow(XSSFSheet sheet, ExcelGenerationResult result) {
        Row row = sheet.createRow(result.getTotalNumberOfRowsGenerated());
        result.incrementTotalRowsGenerated();
        return row;
    }

    private static void enableFiltersInHeaderRow(XSSFWorkbook workbook, XSSFSheet sheet,
            ExcelGenerationConfiguration configuration, ExcelGenerationResult result) {
        if (configuration.headerRowHasAutoFilter() && configuration.shouldIncludeHeaderRow()) {
            int firstRow = result.getHeaderRowIndex();
            int lastRow = firstRow + result.getNumberOfDataRowsGenerated();
            CellRangeAddress range = new CellRangeAddress(firstRow, lastRow, COLUMN_A,
                    configuration.getColumnDefinitions().size() - 1);
            sheet.setAutoFilter(range);
        }
    }

    private static void generateHeaderRow(XSSFWorkbook wb, XSSFSheet sheet, ExcelGenerationConfiguration configuration,
            ExcelGenerationResult result) {
        if (configuration.shouldIncludeHeaderRow()) {
            result.setHeaderRowIndex(result.getTotalNumberOfRowsGenerated());
            Row row = createNewRow(sheet, result);
            for (ExcelColumnDefinition columnDefinition : configuration.getColumnDefinitions()) {
                generateHeaderCell(wb, row, columnDefinition);
            }
        }
    }

    private static void writeWorkbookToFile(ExcelGenerationConfiguration configuration, ExcelGenerationResult result,
            XSSFWorkbook workbook) {
        try {
            File targetFile = configuration.getTargetFile();
            targetFile.getParentFile().mkdirs();
            FileUtils.deleteQuietly(targetFile);
            result.setGeneratedFile(targetFile);

            FileOutputStream outputStream = new FileOutputStream(targetFile);
            workbook.write(outputStream);
            workbook.close();
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    private static void generateDataRows(XSSFWorkbook wb, XSSFSheet sheet, ExcelGenerationConfiguration configuration,
            ExcelGenerationResult result, Object[][] dataTable) {
        LOGGER.debug("Generating Excel...");
        result.setFirstDataRowIndex();
        for (Object[] dataRow : dataTable) {
            Row row = createNewRow(sheet, result);
            result.incrementDataRowsGenerated();
            for (int columnIndex = 0; columnIndex < dataRow.length; columnIndex++) {
                Object value = dataRow[columnIndex];
                generateCell(wb, row, columnIndex, configuration, value);
            }
        }
        result.setLastDataRowIndex();
    }

    private static void generateCell(XSSFWorkbook wb, Row row, int columnIndex,
            ExcelGenerationConfiguration configuration, Object value) {
        Cell cell = row.createCell(columnIndex);

        List<ExcelColumnDefinition> columnDefinitions = configuration.getColumnDefinitions();
        if (columnDefinitions != null) {
            ExcelColumnDefinition columnDefinition = columnDefinitions.get(columnIndex);
            cell.setCellStyle(columnDefinition.getDataStyle(wb));
        }

        if (value instanceof String) {
            cell.setCellValue((String) value);
        } else if (value instanceof Integer) {
            cell.setCellValue((Integer) value);
        } else if (value instanceof Date) {
            cell.setCellValue((Date) value);
        } else {
            cell.setCellValue(value.toString());
        }
    }

    private static void generateHeaderCell(XSSFWorkbook wb, Row row, ExcelColumnDefinition columnDefinition) {
        Cell cell = row.createCell(columnDefinition.getPositionIndex());
        cell.setCellStyle(columnDefinition.getHeaderStyle(wb));
        cell.setCellValue(columnDefinition.getColumnHeading());
    }
}
