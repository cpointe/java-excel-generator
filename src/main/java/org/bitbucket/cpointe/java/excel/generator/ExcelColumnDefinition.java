package org.bitbucket.cpointe.java.excel.generator;

import org.apache.poi.hssf.util.HSSFColor;
import org.apache.poi.ss.usermodel.BorderStyle;
import org.apache.poi.ss.usermodel.CellStyle;
import org.apache.poi.ss.usermodel.FillPatternType;
import org.apache.poi.ss.usermodel.Font;
import org.apache.poi.ss.usermodel.HorizontalAlignment;
import org.apache.poi.ss.usermodel.IndexedColors;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;

public class ExcelColumnDefinition {

    private String columnHeading;
    // could be SUM, AVG, or other excel formulas
    // will result in doing the operation on all data cells in that column (i.e.
    // SUM(A2:A35) )
    private String columnTotalFormula;
    private CellStyle headerStyle;
    private CellStyle dataStyle;
    private CellStyle footerStyle;
    private CellStyle totalStyle;
    private String dataType;
    private int columnPositionIndex;

    public ExcelColumnDefinition(int columnPositionIndex, String columnHeading) {
        this.columnPositionIndex = columnPositionIndex;
        this.columnHeading = columnHeading;
    }

    public String getColumnTotalFormula() {
        return columnTotalFormula;
    }

    public void setColumnTotalFormula(String columnTotalFormula) {
        this.columnTotalFormula = columnTotalFormula;
    }

    /**
     * create a library of cell styles
     */
    public CellStyle getHeaderStyle(Workbook wb) {
        if (headerStyle == null) {
            Font headerFont = wb.createFont();
            headerFont.setBold(true);
            headerStyle = createBorderedStyle(wb);
            headerStyle.setAlignment(HorizontalAlignment.CENTER);
            headerStyle.setFillForegroundColor(IndexedColors.LIGHT_GREEN.getIndex());
            headerStyle.setFillPattern(FillPatternType.SOLID_FOREGROUND);
            headerStyle.setFont(headerFont);
        }
        return headerStyle;
    }

    public CellStyle getDataStyle(Workbook wb) {
        if (dataStyle == null) {
            dataStyle = createBorderedStyle(wb);
            dataStyle.setAlignment(HorizontalAlignment.LEFT);
            dataStyle.setWrapText(true);
        }
        return dataStyle;
    }

//    public CellStyle getFooterStyle(Workbook wb) {
//        if (footerStyle == null) {
//            footerStyle = createBorderedStyle(wb);
//            footerStyle.setAlignment(HorizontalAlignment.RIGHT);
//            footerStyle.setFillBackgroundColor(IndexedColors.CORAL.getIndex());
//        }
//        return footerStyle;
//    }

    private static CellStyle createBorderedStyle(Workbook wb) {
        BorderStyle thin = BorderStyle.THIN;
        short black = IndexedColors.BLACK.getIndex();

        CellStyle style = wb.createCellStyle();
        style.setBorderRight(thin);
        style.setRightBorderColor(black);
        style.setBorderBottom(thin);
        style.setBottomBorderColor(black);
        style.setBorderLeft(thin);
        style.setLeftBorderColor(black);
        style.setBorderTop(thin);
        style.setTopBorderColor(black);
        return style;
    }

    public String getColumnHeading() {
        return columnHeading;
    }

    public void setColumnHeading(String columnHeading) {
        this.columnHeading = columnHeading;
    }

    public String getDataType() {
        return dataType;
    }
    
    public void setDataType(String dataType) {
        this.dataType = dataType;
    }

    public int getPositionIndex() {
        return columnPositionIndex;
    }

    public void setColumnPositionIndex(int columnPositionIndex) {
        this.columnPositionIndex = columnPositionIndex;
    }

    public CellStyle getTotalStyle(XSSFWorkbook wb) {
        if (totalStyle == null) {
            totalStyle = createBorderedStyle(wb);
            totalStyle.setAlignment(HorizontalAlignment.RIGHT);
            totalStyle.setFillForegroundColor(IndexedColors.GREY_50_PERCENT.getIndex());
            totalStyle.setFillPattern(FillPatternType.SOLID_FOREGROUND);
            
            Font font = wb.createFont();
            font.setColor(HSSFColor.HSSFColorPredefined.WHITE.getIndex());
            font.setBold(true);
            font.setItalic(true);
            totalStyle.setFont(font);
        }
        return totalStyle;
    }
}
