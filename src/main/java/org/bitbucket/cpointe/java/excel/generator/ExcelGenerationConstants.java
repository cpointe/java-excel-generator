package org.bitbucket.cpointe.java.excel.generator;

import java.util.HashMap;
import java.util.Map;

public class ExcelGenerationConstants {

    public static final int COLUMN_A = 0;
    public static final int FIRST_ROW = 0;
    public static final String FORMULA_SUM = "SUM";
    public static final String FORMULA_AVERAGE = "AVERAGE";
    public static final String EQUALS = "=";
    public static final String OPEN_PAREN = "(";
    public static final String CLOSE_PAREN = ")";
    public static final String COLON = ":";
    
    public static String getFormulaHeader(String formula) {
        Map<String, String> headerMap = new HashMap<>();
        headerMap.put(FORMULA_SUM, "Total:");
        headerMap.put(FORMULA_AVERAGE, "Average:");
        return headerMap.get(formula);
    }
    
}
