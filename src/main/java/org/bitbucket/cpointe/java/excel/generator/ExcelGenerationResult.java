package org.bitbucket.cpointe.java.excel.generator;

import java.io.File;

public class ExcelGenerationResult {

    protected File generatedFile;
    protected int numberOfDataRowsGenerated = 0;
    private int totalNumberOfRowsGenerated = 0;
    private int headerRowIndex;
    private boolean logoAdded;
    private int firstDataRowIndex;
    private int lastDataRowIndex;

    public void incrementDataRowsGenerated() {
        numberOfDataRowsGenerated++;
    }

    public void incrementTotalRowsGenerated() {
        this.totalNumberOfRowsGenerated++;
    }

    public File getGeneratedFile() {
        return generatedFile;
    }

    public void setGeneratedFile(File generatedFile) {
        this.generatedFile = generatedFile;
    }

    public int getNumberOfDataRowsGenerated() {
        return numberOfDataRowsGenerated;
    }

    public int getTotalNumberOfRowsGenerated() {
        return totalNumberOfRowsGenerated;
    }

    public void setHeaderRowIndex(int headerRowIndex) {
        this.headerRowIndex = headerRowIndex;
    }

    public int getHeaderRowIndex() {
        return headerRowIndex;
    }

    public void incrementTotalRowsGenerated(int numberOfRowsToIncrementBy) {
        for(int i=0; i<numberOfRowsToIncrementBy; i++) {
            incrementTotalRowsGenerated();
        }
    }

    public void setLogoAdded(boolean logoAdded) {
        this.logoAdded = logoAdded;
    }
    
    public boolean isLogoAdded() {
        return logoAdded;
    }

    public int getFirstDataRowIndex() {
        return firstDataRowIndex;
    }
    
    public void setFirstDataRowIndex() {
        this.firstDataRowIndex = getTotalNumberOfRowsGenerated();
    }

    public int getLastDataRowIndex() {
        return lastDataRowIndex;
    }

    public void setLastDataRowIndex() {
        this.lastDataRowIndex = getTotalNumberOfRowsGenerated()-1;
    }

}
