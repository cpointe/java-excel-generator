package org.bitbucket.cpointe.java.excel.generator;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertTrue;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

import org.apache.commons.io.FileUtils;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.CellStyle;
import org.apache.poi.ss.usermodel.Color;
import org.apache.poi.ss.util.CellReference;
import org.apache.poi.xssf.usermodel.XSSFClientAnchor;
import org.apache.poi.xssf.usermodel.XSSFDrawing;
import org.apache.poi.xssf.usermodel.XSSFPicture;
import org.apache.poi.xssf.usermodel.XSSFRow;
import org.apache.poi.xssf.usermodel.XSSFShape;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;

import cucumber.api.java.After;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;

public class DefaultGenerationSteps {

    private static final int NO_HEADER_ROW_IN_INPUT = 0;
    private static final int HEADER_ROW_IN_INPUT = 1;
    private static final String DATE_INPUT_FORMAT = "yyyy-MM-dd HH:mm:ss";
    private static final String RECENT_TIME_FORMAT = "yyyy-MM-dd_HH:mm";
    private static final String ON = "on";
    private ExcelGenerationConfiguration generationConfiguration = new ExcelGenerationConfiguration();;
    private ExcelGenerationResult generationResult;
    private Object[][] testData = { { "Datatype", "Type", "Size(in bytes)" }, { "int", "Primitive", 2 },
            { "float", "Primitive", 4 }, { "double", "Primitive", 8 }, { "char", "Primitive", 1 },
            { "String", "Non-Primitive", "No fixed size" } };
    private File targetTestFile = new File("./target/test-file.xlsx");

    @After
    public void after() {
        if (generationResult != null && generationResult.getGeneratedFile() != null) {
            FileUtils.deleteQuietly(generationResult.getGeneratedFile());
        }
        FileUtils.deleteQuietly(targetTestFile);
    }

    @Given("^I have not changed anything in the generation configuration$")
    public void i_have_not_changed_anything_in_the_generation_configuration() throws Throwable {
        // nothing to do
    }

    @Given("^I configure the generation to generate to the target directory$")
    public void i_configure_the_generation_to_generate_to_the_target_directory() throws Throwable {
        generationConfiguration.setFile(targetTestFile);
    }

    @Given("^I configure the generation to generate to a file called \"([^\"]*)\"$")
    public void i_configure_the_generation_to_generate_to_a_file_called(String targetFileName) throws Throwable {
        generationConfiguration.setTargetFileName(targetFileName);
    }

    @Given("^I configure the generation to \"([^\"]*)\" for the timestamp$")
    public void i_configure_the_generation_to_for_the_timestamp(String isOn) throws Throwable {
        boolean timestampOnFileName = ON.equals(isOn);
        generationConfiguration.setTargetFileNameAddTimestamp(timestampOnFileName);
    }

    @Given("^I configure that the generation to generate to the target directory$")
    public void i_configurat_the_generation_to_generate_to_the_target_directory() throws Throwable {
        generationConfiguration.setTargetFolder("./target");
    }

    @Given("^I configure the generation to turn \"([^\"]*)\" the temp directory$")
    public void i_configure_the_generation_to_turn_the_temp_directory(String useTempOn) throws Throwable {
        boolean useTempDir = ON.equals(useTempOn);
        generationConfiguration.setTargetFolderUseTempDir(useTempDir);
    }

    @Given("^I configure the generation to have a sheet name of \"([^\"]*)\"$")
    public void i_configure_the_generation_to_have_a_sheet_name_of(String sheetName) throws Throwable {
        generationConfiguration.setSheetName(sheetName);
    }

    @Given("^I configure the generation to turn \"([^\"]*)\" the header row$")
    public void i_configure_the_generation_to_turn_the_header_row(String headerRowOn) throws Throwable {
        boolean includeHeader = ON.equals(headerRowOn);
        generationConfiguration.setIncludeHeaderRow(includeHeader);
    }

    @Given("^I configure the generation to turn \"([^\"]*)\" the filters$")
    public void i_configure_the_generation_to_turn_the_filters(String filtersOn) throws Throwable {
        boolean turnFilterOn = ON.equals(filtersOn);
        generationConfiguration.setHeaderRowHasAutoFilter(turnFilterOn);
    }

    @Given("^I have set a title of \"([^\"]*)\" in the configuration$")
    public void i_have_set_a_title_of_in_the_configuration(String title) throws Throwable {
        generationConfiguration.setTitle(title);
    }

    @Given("^I have set a description of \"([^\"]*)\" in the configuration$")
    public void i_have_set_a_description_of_in_the_configuration(String description) throws Throwable {
        generationConfiguration.setDesctiprion(description);
    }

    @Given("^I have set the title buffer to \"([^\"]*)\" lines$")
    public void i_have_set_the_title_buffer_to_lines(int titleBuffer) throws Throwable {
        generationConfiguration.setTitleBuffer(titleBuffer);
    }

    @Given("^I have set an icon of \"([^\"]*)\" in the configuration$")
    public void i_have_set_an_icon_of_in_the_configuration(String logoLocation) throws Throwable {
        generationConfiguration.setLogoLocation(logoLocation);
    }
    
    @Given("^the following column definitions$")
    public void the_following_column_definitions(List<TestExcelRows> data) throws Throwable {
        String[] headers = convertDataToHeaders(data);
        generationConfiguration.setHeaders(headers);
    }

    @Given("^I have configured totals on for the \"([^\"]*)\" collumn$")
    public void i_have_configured_totals_on_for_the_collumn(String columnName) throws Throwable {
        List<ExcelColumnDefinition> columnDefinitions = generationConfiguration.getColumnDefinitions();
        ExcelColumnDefinition columnDefinition = columnDefinitions.stream().filter(c -> columnName.equals(c.getColumnHeading())).findFirst().orElse(null);
        columnDefinition.setColumnTotalFormula(ExcelGenerationConstants.FORMULA_SUM);
    }
    
    @When("^I run the generate a new excel file with the following data$")
    public void i_run_the_generate_a_new_excel_file_with_the_following_data(List<TestExcelRows> data) throws Throwable {
        String[] headers = convertDataToHeaders(data);
        generationConfiguration.setHeaders(headers);
        Object[][] dataRows = convertDataToData(data, HEADER_ROW_IN_INPUT);
        generationResult = ExcelGeneration.generate(generationConfiguration, dataRows);
    }

    @When("^I run the generate a new excel file with the following data with the headers already set$")
    public void i_run_the_generate_a_new_excel_file_with_the_following_data_with_the_headers_already_set(List<TestExcelRows> data) throws Throwable {
        Object[][] dataRows = convertDataToData(data, NO_HEADER_ROW_IN_INPUT);
        generationResult = ExcelGeneration.generate(generationConfiguration, dataRows);
    }
    
    private Object[][] convertDataToData(List<TestExcelRows> dataInputs, int headerRowOffset) throws ParseException {
        Object[][] data = new Object[dataInputs.size() - headerRowOffset][4];
        for (int i = 0; i < data.length; i++) {
            // dataInput row has header line
            Object[] dataRow = data[i];
            TestExcelRows dataInputRow = dataInputs.get(i+headerRowOffset);

            dataRow[0] = dataInputRow.column1;
            dataRow[1] = Integer.parseInt(dataInputRow.intColumn);
            dataRow[2] = dataInputRow.column3;
            dataRow[3] = new SimpleDateFormat(DATE_INPUT_FORMAT).parse(dataInputRow.dateColumn);
        }
        return data;
    }

    private String[] convertDataToHeaders(List<TestExcelRows> data) {
        String[] headers = new String[4];
        headers[0] = data.get(0).column1;
        headers[1] = data.get(0).intColumn;
        headers[2] = data.get(0).column3;
        headers[3] = data.get(0).dateColumn;
        return headers;
    }

    @When("^I run the generate a new excel file$")
    public void i_run_the_generate_a_new_excel_file() throws Throwable {
        generationResult = ExcelGeneration.generate(generationConfiguration, testData);
    }

    @Then("^I should get back a results object with the generated file location$")
    public void i_should_get_back_a_results_object_with_the_generated_file_location() throws Throwable {
        assertNotNull("The generation returned a null file", generationResult.getGeneratedFile());
        assertTrue("The file does not appear to have generated", generationResult.getGeneratedFile().exists());
    }

    @Then("^the file should exist in the target directory$")
    public void the_file_should_exist_in_the_target_directory() throws Throwable {
        assertTrue(
                "File doesn't appear to have generated in the target directory \n\t generated: "
                        + generationResult.getGeneratedFile() + "\n\t targeted: " + targetTestFile,
                targetTestFile.exists());
    }

    @Then("^the file name should end with \"([^\"]*)\"$")
    public void the_file_name_should_end_with(String ending) throws Throwable {
        String fileName = generationResult.getGeneratedFile().getAbsolutePath();
        assertTrue("The file name (" + fileName + ") didn't end with (" + ending + ")", fileName.endsWith(ending));
    }

    @Then("^cell at \"([^\"]*)\" should have \"([^\"]*)\" as the value$")
    public void row_column_should_have_as_the_value(String cellIdentifier, String expectedValue) throws Throwable {
        XSSFWorkbook workbook = openGeneratedWorkbook();
        XSSFSheet sheet = workbook.getSheetAt(0);

        Cell cell = getCellByIdentifier(sheet, cellIdentifier);
        String cellValue;
        
        if(Cell.CELL_TYPE_NUMERIC == cell.getCellType()) {
            cellValue = Double.toString(cell.getNumericCellValue());
        } else if (Cell.CELL_TYPE_FORMULA == cell.getCellType()) {
            cellValue = cell.getCellFormula();
        } else {
            cellValue = cell.getStringCellValue();
        }
        
        assertEquals("The values didn't match.", expectedValue, cellValue);
        workbook.close();
    }

    private Cell getCellByIdentifier(XSSFSheet sheet, String cellIdentifier) {
        CellReference cellReference = new CellReference(cellIdentifier);
        XSSFRow row = sheet.getRow(cellReference.getRow());
        Cell cell = row.getCell(cellReference.getCol());
        return cell;
    }

    private XSSFWorkbook openGeneratedWorkbook() throws FileNotFoundException, IOException {
        FileInputStream excelFile = new FileInputStream(generationResult.getGeneratedFile());
        XSSFWorkbook workbook = new XSSFWorkbook(excelFile);
        return workbook;
    }

    @Then("^the file name should end with the current date and time$")
    public void the_file_name_should_end_with_the_current_date_and_time() throws Throwable {
        String fileName = generationResult.getGeneratedFile().getAbsolutePath();
        String expectedDate = new SimpleDateFormat(RECENT_TIME_FORMAT).format(new Date());
        assertTrue(
                "The date on the file (" + fileName + ") doesn't look correct. Should include (" + expectedDate + ").",
                fileName.contains(expectedDate));
    }

    @Then("^the sheet should have a name of \"([^\"]*)\"$")
    public void the_sheet_should_have_a_name_of(String sheetName) throws Throwable {
        XSSFSheet sheet = getPrimarySheet();
        assertEquals("Sheet name didn't come out correctly", sheetName, sheet.getSheetName());
    }

    private XSSFSheet getPrimarySheet() throws FileNotFoundException, IOException {
        XSSFWorkbook workbook = openGeneratedWorkbook();
        XSSFSheet sheet = workbook.getSheetAt(0);
        return sheet;
    }

    @Then("^cell \"([^\"]*)\" has a color$")
    public void cell_has_a_color(String cellIdentifier) throws Throwable {
        XSSFSheet sheet = getPrimarySheet();
        Cell cell = getCellByIdentifier(sheet, cellIdentifier);
        CellStyle cellStyle = cell.getCellStyle();
        Color cellColor = cellStyle.getFillForegroundColorColor();
        assertNotNull("Cell color was null.", cellColor);
    }

    @Then("^cell \"([^\"]*)\" has the filter turned \"([^\"]*)\"$")
    public void cell_has_the_filter_turned(String cellIdentifier, String expectedFilterOn) throws Throwable {
        XSSFSheet sheet = getPrimarySheet();
        boolean sheetFilterOn = sheet.getCTWorksheet().isSetAutoFilter();
        assertEquals("The filter is not correct", ON.equals(expectedFilterOn), sheetFilterOn);
    }

    @Then("^cell at \"([^\"]*)\" should have and image$")
    public void cell_at_should_have_and_image(String cellIdentifier) throws Throwable {
        XSSFSheet sheet = getPrimarySheet();
        CellReference cellReference = new CellReference(cellIdentifier);

        // https://stackoverflow.com/questions/29671780/get-the-image-position-location-of-an-image-in-xlsx-file-using-apache-poi
        XSSFDrawing dp = sheet.createDrawingPatriarch();
        List<XSSFShape> pics = dp.getShapes();
        XSSFPicture inpPic = (XSSFPicture) pics.get(0);
        XSSFClientAnchor clientAnchor = inpPic.getClientAnchor();

        assertEquals("The logo's column was not correct!", clientAnchor.getCol1(), cellReference.getCol());
        assertEquals("The logo's row was not correct!", clientAnchor.getRow1(), cellReference.getRow());
    }

    @Then("^cell at \"([^\"]*)\" should have no value$")
    public void cell_at_should_have_no_value(String cellIdentifier) throws Throwable {
        XSSFSheet sheet = getPrimarySheet();
        CellReference cellReference = new CellReference(cellIdentifier);
        XSSFRow row = sheet.getRow(cellReference.getRow());
        assertNull("The row should not exist!", row);
    }
}

class TestExcelRows {
    public String column1;
    public String intColumn;
    public String column3;
    public String dateColumn;
}