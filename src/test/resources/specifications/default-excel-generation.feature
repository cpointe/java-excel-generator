@dev
Feature: Excel Generation -> Basics
  If nothing is passed in for the configurations then verify that the generation process generates a sample file.

  # useful documentation: https://poi.apache.org/components/spreadsheet/quick-guide.html#Images
  Scenario: Generate an excel file
    Given I have not changed anything in the generation configuration
    When I run the generate a new excel file
    Then I should get back a results object with the generated file location

  Scenario: Generate with no data
    When I run the generate a new excel file with the following data
      | column1    | intColumn        | column3 | dateColumn    |
      | header row | my second header | header3 | test header 4 |
    Then cell "A1" has the filter turned "on"
    And cell at "A1" should have "header row" as the value
    And cell at "A2" should have no value

  ############# Generation File Scenarios #############
  Scenario: Generate into specific directory
    Given I configure the generation to generate to the target directory
    When I run the generate a new excel file
    Then the file should exist in the target directory

  Scenario: Generate with desired file name
    Given I configure the generation to generate to a file called "my-file"
    And I configure the generation to turn "off" the temp directory
    And I configure that the generation to generate to the target directory
    And I configure the generation to "of" for the timestamp
    When I run the generate a new excel file
    Then the file name should end with "my-file.xlsx"

  Scenario: Generate and override previous file
    Given I configure the generation to generate to a file called "my-file"
    And I configure the generation to turn "off" the temp directory
    And I configure that the generation to generate to the target directory
    And I configure the generation to "off" for the timestamp
    When I run the generate a new excel file with the following data
      | column1    | intColumn        | column3                 | dateColumn          |
      | header row | my second header | header3                 | test header 4       |
      | data row 1 |                2 | this is a string column | 2018-01-01 13:14:32 |
      | data row 2 |                3 | this is a string column | 2018-01-01 13:14:32 |
    And I run the generate a new excel file with the following data
      | column1      | intColumn | column3                 | dateColumn          |
      | header1      | header2   | header3                 | header4             |
      | updated data |         2 | this is a string column | 2018-01-01 13:14:32 |
      | data row 2   |         3 | this is a string column | 2018-01-01 13:14:32 |
    Then the file name should end with "my-file.xlsx"
    And cell at "A2" should have "updated data" as the value

  Scenario: Generate with timestamp in file name
    Given I configure the generation to generate to a file called "my-file"
    And I configure the generation to turn "off" the temp directory
    And I configure that the generation to generate to the target directory
    And I configure the generation to "on" for the timestamp
    When I run the generate a new excel file
    Then the file name should end with the current date and time

  ############# Generation Sheet Name Scenarios #############
  Scenario: Generate with desired sheet name
    Given I configure the generation to have a sheet name of "my awesome data"
    When I run the generate a new excel file
    Then the sheet should have a name of "my awesome data"

  ############# Generation Header Scenarios #############
  Scenario: Generate header row
    Given I configure the generation to turn "on" the header row
    When I run the generate a new excel file with the following data
      | column1         | intColumn | column3                 | dateColumn          |
      | header1         | header2   | header3                 | header4             |
      | with header row |         2 | this is a string column | 2018-01-01 13:14:32 |
    Then cell at "A2" should have "with header row" as the value

  Scenario: Generate header row DISABLED
    Given I configure the generation to turn "off" the header row
    When I run the generate a new excel file with the following data
      | column1            | intColumn | column3                 | dateColumn          |
      | header1            | header2   | header3                 | header4             |
      | without header row |         2 | this is a string column | 2018-01-01 13:14:32 |
    Then cell at "A1" should have "without header row" as the value

  # https://stackoverflow.com/questions/11529542/changing-cell-color-using-apache-poi
  Scenario: Generate with specific color in header row
    # generating the header is on by default
    When I run the generate a new excel file with the following data
      | column1         | intColumn | column3                 | dateColumn          |
      | header1         | header2   | header3                 | header4             |
      | with header row |         2 | this is a string column | 2018-01-01 13:14:32 |
    Then cell at "A2" should have "with header row" as the value
    # not easy to test the color type
    And cell "A1" has a color

  # https://stackoverflow.com/questions/13703441/setting-filter-on-headers-of-an-excel-sheet-via-poi
  # gold mine: http://svn.apache.org/repos/asf/poi/trunk/src/examples/src/org/apache/poi/ss/examples/BusinessPlan.java
  Scenario: Generate with filters in header row
    # header row & filters are on by default
    When I run the generate a new excel file with the following data
      | column1         | intColumn | column3                 | dateColumn          |
      | header1         | header2   | header3                 | header4             |
      | with header row |         2 | this is a string column | 2018-01-01 13:14:32 |
    Then cell at "A2" should have "with header row" as the value
    And cell "A1" has the filter turned "on"

  Scenario: Generate with filters in header row DISABLED
    Given I configure the generation to turn "off" the filters
    When I run the generate a new excel file with the following data
      | column1         | intColumn | column3                 | dateColumn          |
      | header1         | header2   | header3                 | header4             |
      | with header row |         2 | this is a string column | 2018-01-01 13:14:32 |
    Then cell at "A2" should have "with header row" as the value
    And cell "A1" has the filter turned "off"

  Scenario: Generate with a title in first row
    # Title only enabled if set to a value. If null, no title is added.
    Given I have set a title of "Hello World" in the configuration
    When I run the generate a new excel file with the following data
      | column1     | intColumn | column3                 | dateColumn          |
      | header1     | header2   | header3                 | header4             |
      | with header |         2 | this is a string column | 2018-01-01 13:14:32 |
    Then cell at "A1" should have "Hello World" as the value
    # Verify that there are 2 spacing lines
    And cell "A4" has the filter turned "on"
    And cell at "A4" should have "header1" as the value
    And cell at "A5" should have "with header" as the value

  Scenario: Generate with description
    # Description only enabled if set to a value. If null, no description is added.
    Given I have set a description of "This is my first excel file." in the configuration
    When I run the generate a new excel file with the following data
      | column1     | intColumn | column3                 | dateColumn          |
      | header1     | header2   | header3                 | header4             |
      | with header |         2 | this is a string column | 2018-01-01 13:14:32 |
    Then cell at "A1" should have "This is my first excel file." as the value
    # Verify that there are 2 spacing lines
    And cell "A4" has the filter turned "on"
    And cell at "A4" should have "header1" as the value
    And cell at "A5" should have "with header" as the value

  Scenario: Generate with title and description
    Given I have set a title of "Hello World" in the configuration
    And I have set a description of "This is my first excel file." in the configuration
    When I run the generate a new excel file with the following data
      | column1     | intColumn | column3                 | dateColumn          |
      | header1     | header2   | header3                 | header4             |
      | with header |         2 | this is a string column | 2018-01-01 13:14:32 |
    Then cell at "A1" should have "Hello World" as the value
    And cell at "A2" should have "This is my first excel file." as the value
    # Verify that there are 2 spacing lines
    And cell "A5" has the filter turned "on"
    And cell at "A5" should have "header1" as the value
    And cell at "A6" should have "with header" as the value

  Scenario: Increase the title buffer
    Given I have set a title of "Hello World" in the configuration
    And I have set the title buffer to "20" lines
    When I run the generate a new excel file with the following data
      | column1     | intColumn | column3                 | dateColumn          |
      | header1     | header2   | header3                 | header4             |
      | with header |         2 | this is a string column | 2018-01-01 13:14:32 |
    Then cell at "A1" should have "Hello World" as the value
    # Verify that there are 20 spacing lines
    And cell "A22" has the filter turned "on"
    And cell at "A22" should have "header1" as the value
    And cell at "A23" should have "with header" as the value

  Scenario: No title buffer
    Given I have set a title of "Hello World" in the configuration
    And I have set the title buffer to "0" lines
    When I run the generate a new excel file with the following data
      | column1     | intColumn | column3                 | dateColumn          |
      | header1     | header2   | header3                 | header4             |
      | with header |         2 | this is a string column | 2018-01-01 13:14:32 |
    Then cell at "A1" should have "Hello World" as the value
    # Verify that there are 20 spacing lines
    And cell "A2" has the filter turned "on"
    And cell at "A2" should have "header1" as the value
    And cell at "A3" should have "with header" as the value

  # https://stackoverflow.com/questions/21991473/apache-poi-insert-image
  Scenario: Generate with logo in header
    Given I have set an icon of "src/test/resources/test-logo.png" in the configuration
    When I run the generate a new excel file with the following data
      | column1     | intColumn | column3                 | dateColumn          |
      | header1     | header2   | header3                 | header4             |
      | with header |         2 | this is a string column | 2018-01-01 13:14:32 |
    Then cell at "A1" should have and image
    # Verify that there are 2 spacing lines
    And cell "A3" has the filter turned "on"
    And cell at "A3" should have "header1" as the value
    And cell at "A4" should have "with header" as the value

  Scenario: Generate with logo in header, title, and description
    Given I have set an icon of "src/test/resources/test-logo.png" in the configuration
    And I have set a title of "Hello World" in the configuration
    And I have set a description of "This is my first excel file." in the configuration
    When I run the generate a new excel file with the following data
      | column1     | intColumn | column3                 | dateColumn          |
      | header1     | header2   | header3                 | header4             |
      | with header |         2 | this is a string column | 2018-01-01 13:14:32 |
    Then cell at "A1" should have and image
    And cell at "B1" should have "Hello World" as the value
    And cell at "B2" should have "This is my first excel file." as the value
    # Verify that there are 2 spacing lines
    And cell "A5" has the filter turned "on"
    And cell at "A5" should have "header1" as the value
    And cell at "A6" should have "with header" as the value

  ############# Generation with Footer Scenarios #############
  Scenario: Generate with column totals at bottom
    Given the following column definitions
      | column1     | intColumn      | column3                 | dateColumn          |
      | header1     | totaled column | header3                 | header4             |
    And I have configured totals on for the "totaled column" collumn
    When I run the generate a new excel file with the following data with the headers already set
      | column1     | intColumn | column3                 | dateColumn          |
      | with header |              1 | this is a string column | 2018-01-01 13:14:32 |
      | with header |              2 | this is a string column | 2018-01-01 13:14:32 |
      | with header |              3 | this is a string column | 2018-01-01 13:14:32 |
      | with header |              4 | this is a string column | 2018-01-01 13:14:32 |
      | with header |              5 | this is a string column | 2018-01-01 13:14:32 |
    # Verify that there are 2 spacing lines
    And cell at "B9" should have "Total:" as the value
    And cell at "B10" should have "SUM(B2:B6)" as the value


  ############# Generation with Freeze Panes Scenarios #############
  # https://stackoverflow.com/questions/17932575/apache-poi-locking-header-rows
  Scenario: Generate with frozen header row

  Scenario: Generate with first column frozen

  Scenario: Generate with specificly frozen cell

  ############# Data Input Scenarios #############
  Scenario: Genearte from data as objects

  Scenario: Generate from data as POJOs

  Scenario: Generate from stream of objects

  Scenario: Generate from stream of POJOs
